<?php

namespace App\Http\Controllers;

use App\Job;
use App\User;
use App\State;
use App\Country;
use App\Category;
use App\Industry;
use App\CompanyRequest;
use App\JobApplication;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Notifications\BlockUser;
use App\Notifications\UnblockUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use App\Notifications\Request_For_Profile;

class UserController extends Controller
{

    public function __construct()
    {
        //
    }

    public function index()
    {
        $title = trans('app.users');
        $current_user = Auth::user();

        $users = User::where('id', '!=', $current_user->id)->orderBy('name', 'asc')->paginate(20);
        return view('admin.users', compact('title', 'users'));
    }

    public function Userindex()
    {
        $users = User::where('user_type', 'user')->where('active_status', 1)->get();
        $countries = Country::all();
        $categories = Category::all();
        $jobs = Job::all();
        return view('admin.users.index', compact('users', 'countries', 'categories', 'jobs'));
    }

    public function singleuser($id)
    {
        $user = User::find($id);

        return view('admin.users.single', compact('user'));
    }


    public function request($id)
    {
        $jobseeker = User::where(['id' => $id])->first();

        $title = trans('app.profile_edit');
        $user = Auth::user();


        return view('admin.users.requestuser', compact('title', 'user', 'id', 'jobseeker'));
    }

    public function storeRequest(Request $request, $id)
    {

        $jobseeker = User::where(['id' => $id])->first();
        $data = $request->input();
        CompanyRequest::create([
            'company' => $data['company'],
            'job_seeker' => $data['jobseeker'],
            'company_email' => $data['email'],
            'job_seeker_id' => $id,
            'status' => 0,
        ]);



        return back()->with(['message' => 'Request Submited Successfully']);
    }

    public function aprove($id)
    {
        $aprove  = CompanyRequest::find($id);

        $mailuser = User::where('email', $aprove->company_email)->first();



        $jobseeker = User::where('id', $aprove->job_seeker_id)->first();

        $aprove->status = 1;
        $aprove->update();

        $mailuser->notify(new Request_For_Profile($jobseeker));
        return back()->with(['message' => 'Requect Aproved Successfully']);
    }

    public function reject($id)
    {
        $reject  = CompanyRequest::find($id);
        $reject->status = 2;
        $reject->update();
        return back()->with(['message' => 'Request Reject Successfully']);
    }


    public function requestUser($id)
    {
        $user = User::where('id', '=', $id)->first();


        return view('admin.users.single', compact('user'));
    }


    public function deleteUser($id)
    {

        $user = User::find($id);
        $user->delete();
        return redirect()->route('allUsers')->with(['message' => 'User Deleted Successfully']);
    }

    public function blockUser($id)
    {
        $user = User::find($id);
        $user->notify(new BlockUser($user->name . "  You are Blocked."));
        $user->active_status = 2;
        $user->update();
        $user->notify(new BlockUser($user->name . "  You are Blocked."));
        return back()->with(['message' => 'User Blocked Successfully']);
    }

    public function unblockUser($id)
    {
        $user = User::find($id);
        $user->notify(new UnblockUser($user->name . "  You are Unblock."));
        $user->active_status = 1;
        $user->update();
        return back()->with(['message' => 'User Unblock Successfully']);
    }



    public function requestTable()
    {
        $title = trans('Aproved Users');
        $requests = CompanyRequest::where('status', '!=', 2)->paginate(10);
        $requestcount = $requests->count();
        return view('admin.users.requestable', compact('title', 'requests'));
    }

    public function rejectTable()
    {
        $title = trans('Rejected Users');
        $reject_users = CompanyRequest::where('status', '=', 2)->paginate(10);
        return view('admin.users.rejecttable', compact('title', 'reject_users'));
    }


    public function allUser()
    {
        $title = trans('All Users');
        $users = User::where('user_type', '=', 'user')->where('active_status', '=', 1)->paginate(10);
        return view('admin.allusers', compact('title', 'users'));
    }

    public function pendingUsers()
    {
        $title = trans('Pending Users');
        $users = User::where('user_type', '=', 'user')->where('active_status', '=', 0)->paginate(10);
        return view('admin.pendingusers', compact('title', 'users'));
    }

    public function blockedUsers()
    {
        $title = trans('Blocked Users');
        $users = User::where('user_type', '=', 'user')->where('active_status', '=', 2)->paginate(10);
        return view('admin.blockedusers', compact('title', 'users'));
    }

    public function viewUser($id)
    {
        $title = trans('User Profile');
        $user = User::find($id);
        return view('admin.singleU', compact('title', 'user'));
    }

    // company or employee Function

    public function allcompanies()
    {
        $title = trans('All Companies');
        $companies = User::where('user_type', '=', 'employer')->where('active_status', '=', 1)->paginate(10);
        return view('admin.allcompanies', compact('title', 'companies'));
    }

    public function viewCompany($id)
    {
        $title = trans('Company Profile');
        $company = User::find($id);


        return view('admin.singleC', compact('title', 'company'));
    }
    // Reques against user
    public function userRequest()
    {
        $title = trans('Requests');
        $id = Auth::user()->id;
        $requestUsers = CompanyRequest::where('job_seeker_id', '=', $id)->paginate(5);
        return view('admin.users.UserRequest', compact('title', 'requestUsers'));
    }

    public function reject_request()
    {
        $title = trans('Reject Requests');
        $company = CompanyRequest::where('status', '=', 2)->where('job_seeker_id', '=', Auth::user()->id)->paginate(10);
        return view('admin.users.rejectRequest', compact('title', 'company'));
    }

    public function blockCompany($id)
    {
        $company = User::find($id);
        $company->active_status = 2;
        $company->update();
        return back()->with(['message' => 'Company Blocked Successfully']);
    }

    public function deleteCompany($id)
    {
        $company = User::find($id);
        $company->delete();
        return redirect()->route('allcompanies')->with(['message' => 'Company Deleted Successfully']);
    }

    public function blockedCompanies()
    {
        $title = trans('Blocked Companis');
        $companies = User::where('user_type', '=', 'employer')->where('active_status', '=', 2)->paginate(10);
        return view('admin.blockedcompanies', compact('title', 'companies'));
    }


    public function unblockCompany($id)
    {
        $company = User::find($id);
        $company->active_status = 1;
        $company->update();
        return back()->with(['message' => 'Company Unblock Successfully']);
    }

    public function pendingCompanies()
    {
        $title = trans('Pending Companies');
        $companies = User::where('user_type', '=', 'employer')->where('active_status', '=', 0)->paginate(10);
        return view('admin.pendingCompanies', compact('title', 'companies'));
    }

    public function userSearch(Request $request)
    {

        if (!empty($request->ind) && !empty($request->location)) {
            $user_ind = $request->ind;
            $user_location = $request->location;
            $user = User::where('industry', 'like', '%' . $user_ind . '%')
                ->orwhere('skills', 'like', '%' . $user_ind . '%')
                ->where('country_name', 'like', '%' . $user_location . '%')
                ->orwhere('city', 'like', '%' . $user_location . '%')
                ->where('user_type', '=', 'user')
                ->where('active_status', '=', 1)
                ->get();
        }
        $countries = Country::all();
        $categories = Category::all();
        $jobs = Job::all();
        return view('admin.users.user_search', compact('user', 'countries', 'categories', 'jobs', 'user_ind', 'user_location'));
    }


    public function filterSearch(Request $request)
    {

        if (!empty($request->industry) && !empty($request->exp_level) && !empty($request->job_type) && !empty($request->location)) {
            $user_industry = $request->industry;
            $user_exp_level = $request->exp_level;
            $user_job_type = $request->job_type;
            $user_location = $request->location;
            $users = User::where('industry', 'like', '%' . $user_industry . '%')
                ->orwhere('skills', 'like', '%' . $user_industry . '%')
                ->where('country_name', 'like', '%' . $user_location . '%')
                ->orwhere('city', 'like', '%' . $user_location . '%')
                ->where('exp_level', 'like', '%' . $user_exp_level . '%')
                ->where('job_type', 'like', '%' . $user_job_type . '%')
                ->where('user_type', '=', 'user')
                ->where('active_status', '=', 1)
                ->get();
        }

        $countries = Country::all();
        $categories = Category::all();
        $jobs = Job::all();
        return view('admin.users.index', compact('users', 'countries', 'categories', 'jobs'));
    }

    public function singleSearch(Request $request)
    {
        if (!empty($request->industry) && !empty($request->exp_level) && !empty($request->job_type) && !empty($request->location)) {
            $user_industry = $request->industry;
            $user_exp_level = $request->exp_level;
            $user_job_type = $request->job_type;
            $user_location = $request->location;
            $user_ind = $request->ind;
            $user_location = $request->location;
            $user = User::where('industry', 'like', '%' . $user_industry . '%')
                ->orwhere('skills', 'like', '%' . $user_industry . '%')
                ->where('country_name', 'like', '%' . $user_location . '%')
                ->orwhere('city', 'like', '%' . $user_location . '%')
                ->where('exp_level', 'like', '%' . $user_exp_level . '%')
                ->where('job_type', 'like', '%' . $user_job_type . '%')
                ->where('user_type', '=', 'user')
                ->where('active_status', '=', 1)
                ->get();
        }
        $countries = Country::all();
        $categories = Category::all();
        $jobs = Job::all();
        return view('admin.users.user_search', compact('user', 'countries', 'categories', 'jobs', 'user_ind', 'user_location'));
    }

    public function show($id = 0)
    {
        if ($id) {
            $title = trans('app.profile');
            $user = User::find($id);

            $is_user_id_view = true;
            return view('admin.profile', compact('title', 'user', 'is_user_id_view'));
        }
    }

    public function getDownload($id)
    {
        $user = User::find($id);

        //PDF file is stored under project/public/download/info.pdf
        $file = $user->file;
        return Response::download($file);
    }

    /**
     * @param $id
     * @param null $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function statusChange($id, $status = null)
    {
        if (config('app.is_demo')) {
            return redirect()->back()->with('error', 'This feature has been disable for demo');
        }

        $user = User::find($id);
        if ($user && $status) {
            if ($status == 'approve') {
                $user->active_status = 1;
                $user->save();
            } elseif ($status == 'block') {
                $user->active_status = 2;
                $user->save();
            }
        }
        return back()->with('success', trans('app.status_updated'));
    }

    public function appliedJobs()
    {
        $title = __('app.applicant');
        $user_id = Auth::user()->id;
        $applications = JobApplication::whereUserId($user_id)->orderBy('id', 'desc')->paginate(20);

        return view('admin.applied_jobs', compact('title', 'applications'));
    }

    public function registerJobSeeker()
    {
        $title = __('app.register_job_seeker');
        return view('register-job-seeker', compact('title'));
    }

    public function registerJobSeekerPost(Request $request)
    {
        $rules = [
            'name' => ['required', 'string', 'max:190'],
            'email' => ['required', 'string', 'email', 'max:190', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'user_code' => ['unique:users'],
        ];

        $this->validate($request, $rules);

        $data = $request->input();
        User::create([
            'name'          => $data['name'],
            'email'         => $data['email'],
            'user_type'     => 'user',
            'password'      => Hash::make($data['password']),
            'user_code'     => mt_rand(1000, 9999),
            'active_status' => 1,
        ]);

        return redirect(route('login'))->with('success', __('app.registration_successful'));
    }

    public function registerEmployer()
    {
        $title = __('app.employer_register');
        $countries = Country::all();
        $industries = Industry::all();
        $old_country = false;
        if (old('country')) {
            $old_country = Country::find(old('country'));
        }
        return view('employer-register', compact('title', 'countries', 'industries', 'old_country'));
    }

    public function registerEmployerPost(Request $request)
    {
        $email = $request->email;
        $name = Str::contains($email, $request->company);

        if ($name == false) {

            return redirect()->back()->with('error', 'Email Must Be Cotained with Company Name');
        } else
            $rules = [
                'name'      => ['required', 'string', 'max:190'],
                'company'   => 'required',
                'email'     => ['required', 'string', 'email', 'max:190', 'unique:users'],
                'password'  => ['required', 'string', 'min:6', 'confirmed'],
                'phone'     => 'required',
                'address'   => 'required',
                'country'   => 'required',
                'state'     => 'required',
                'industry' => 'required',
                'user_code' => ['unique:users'],
                'company_post' => 'required',

            ];
        $this->validate($request, $rules);

        $company = $request->company;
        $company_slug = unique_slug($company, 'User', 'company_slug');

        $country = Country::find($request->country);
        $state_name = null;
        if ($request->state) {
            $state = State::find($request->state);
            $state_name = $state->state_name;
        }

        User::create([
            'name'          => $request->name,
            'company'       => $company,
            'company_slug'  => $company_slug,
            'email'         => $request->email,
            'user_type'     => 'employer',
            'password'      => Hash::make($request->password),

            'phone'         => $request->phone,
            'address'       => $request->address,
            'address_2'     => $request->address_2,
            'country_id'    => $request->country,
            'country_name'  => $country->country_name,
            'state_id'      => $request->state,
            'state_name'    => $state_name,
            'industry'      => $request->industry,
            'city'          => $request->city,
            'active_status' => 1,
            'user_code'     => 'c' . mt_rand(1000, 9999),
            'company_post' => $request->company_post,
        ]);

        return redirect(route('login'))->with('success', __('app.registration_successful'));
    }


    public function registerAgent()
    {
        $title = __('app.agent_register');
        $countries = Country::all();
        $old_country = false;
        if (old('country')) {
            $old_country = Country::find(old('country'));
        }
        return view('agent-register', compact('title', 'countries', 'old_country'));
    }

    public function registerAgentPost(Request $request)
    {
        $rules = [
            'name'      => ['required', 'string', 'max:190'],
            'company'   => 'required',
            'email'     => ['required', 'string', 'email', 'max:190', 'unique:users'],
            'password'  => ['required', 'string', 'min:6', 'confirmed'],
            'phone'     => 'required',
            'address'   => 'required',
            'country'   => 'required',
            'state'     => 'required',
        ];
        $this->validate($request, $rules);

        $company = $request->company;
        $company_slug = unique_slug($company, 'User', 'company_slug');

        $country = Country::find($request->country);
        $state_name = null;
        if ($request->state) {
            $state = State::find($request->state);
            $state_name = $state->state_name;
        }

        User::create([
            'name'          => $request->name,
            'company'       => $company,
            'company_slug'  => $company_slug,
            'email'         => $request->email,
            'user_type'     => 'agent',
            'password'      => Hash::make($request->password),

            'phone'         => $request->phone,
            'address'       => $request->address,
            'address_2'     => $request->address_2,
            'country_id'    => $request->country,
            'country_name'  => $country->country_name,
            'state_id'      => $request->state,
            'state_name'    => $state_name,
            'city'          => $request->city,
            'active_status' => 1,
        ]);

        return redirect(route('login'))->with('success', __('app.registration_successful'));
    }


    public function employerProfile()
    {
        $title = __('app.employer_profile');
        $user = Auth::user();


        $countries = Country::all();
        $old_country = false;
        if ($user->country_id) {
            $old_country = Country::find($user->country_id);
        }

        return view('admin.employer-profile', compact('title', 'user', 'countries', 'old_country'));
    }

    public function employerProfilePost(Request $request)
    {
        $user = Auth::user();

        $rules = [
            'company_size'   => 'required',
            'phone'     => 'required',
            'address'   => 'required',
            'country'   => 'required',
            'state'     => 'required',
        ];

        $this->validate($request, $rules);


        $logo = null;
        if ($request->hasFile('logo')) {
            $image = $request->file('logo');

            $valid_extensions = ['jpg', 'jpeg', 'png'];
            if (!in_array(strtolower($image->getClientOriginalExtension()), $valid_extensions)) {
                return redirect()->back()->withInput($request->input())->with('error', 'Only .jpg, .jpeg and .png is allowed extension');
            }
            $file_base_name = str_replace('.' . $image->getClientOriginalExtension(), '', $image->getClientOriginalName());
            $resized_thumb = Image::make($image)->resize(256, 256)->stream();

            $logo = strtolower(time() . str_random(5) . '-' . str_slug($file_base_name)) . '.' . $image->getClientOriginalExtension();

            $logoPath = 'uploads/images/logos/' . $logo;

            try {
                Storage::disk('public')->put($logoPath, $resized_thumb->__toString());
            } catch (\Exception $e) {
                return redirect()->back()->withInput($request->input())->with('error', $e->getMessage());
            }
        }

        $country = Country::find($request->country);
        $state_name = null;
        if ($request->state) {
            $state = State::find($request->state);
            $state_name = $state->state_name;
        }

        $data = [
            'company_size'  => $request->company_size,
            'phone'         => $request->phone,
            'address'       => $request->address,
            'address_2'     => $request->address_2,
            'country_id'    => $request->country,
            'country_name'  => $country->country_name,
            'state_id'      => $request->state,
            'state_name'    => $state_name,
            'city'          => $request->city,
            'about_company' => $request->about_company,
            'website'       => $request->website,
        ];

        if ($logo) {
            $data['logo'] = $logo;
        }

        $user->update($data);

        return back()->with('success', __('app.updated'));
    }


    public function employerApplicant()
    {
        $title = __('app.applicant');
        $employer_id = Auth::user()->id;
        $applications = JobApplication::whereEmployerId($employer_id)->orderBy('id', 'desc')->paginate(20);

        return view('admin.applicants', compact('title', 'applications'));
    }

    public function makeShortList($application_id)
    {
        $applicant = JobApplication::find($application_id);
        $applicant->is_shortlisted = 1;
        $applicant->save();
        return back()->with('success', __('app.success'));
    }

    public function shortlistedApplicant()
    {
        $title = __('app.shortlisted');
        $employer_id = Auth::user()->id;
        $applications = JobApplication::whereEmployerId($employer_id)->whereIsShortlisted(1)->orderBy('id', 'desc')->paginate(20);

        return view('admin.applicants', compact('title', 'applications'));
    }


    public function profile()
    {
        $title = trans('app.profile');
        $user = Auth::user();

        return view('admin.profile', compact('title', 'user'));
    }

    // public function userprofile($id = null)
    // {
    //     $title = trans('app.profile_edit');
    //     $user = Auth::user();



    //     if ($id) {
    //         $user = User::find($id);
    //     }

    //     $countries = Country::all();


    //     return view('admin.user_profile_edit', compact('title', 'user', 'countries'));
    // }

    public function profileEdit($id = null)
    {
        $title = trans('app.profile_edit');
        $user = Auth::user();



        if ($id) {
            $user = User::find($id);
        }

        $countries = Country::all();
        $industries = Industry::all();

        return view('admin.profile_edit', compact('title', 'user', 'countries', 'industries'));
    }

    public function userform()
    {
        $title = trans('app.profile_edit');
        $user = Auth::user();
        $countries = Country::all();
        $categories = Category::all();
        $industries = Industry::all();
        return view('admin.user_profile_edit', compact('title', 'user', 'countries', 'categories', 'industries'));
    }

    public function  userprofileupdate(Request $request)
    {
        $user = Auth::user();
        request()->validate([
            'file' => 'required',
            'file.*' => 'mimes:doc,docx,pdf,txt',
            'image' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif'


        ]);

        if ($files = $request->file('file')) {
            $destinationPath = 'document'; // upload path
            $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $fileName);
        }


        $id  = $request->id;
        $user = User::find($id);
        $user->name  = $request->name;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->phone = $request->phone;
        $user->country_name = $request->country_name;
        $user->address = $request->address;
        $user->job_type = $request->job_type;
        $user->industry = $request->industry;
        $user->profiletype = $request->profiletype;
        $user->city = $request->city;
        $user->u_nationality = $request->u_nationality;
        $user->u_experience = $request->u_experience;
        $user->u_about = $request->u_about;
        $user->file = 'document/' . $fileName;
        $user->u_ehistory = $request->u_ehistory;
        $user->skills = $request->category;
        $user->exp_level = $request->exp_level;

        // if ($request->hasfile('image')) {
        //     if (!empty($user->image)) {
        //         $image_path = $user->image;
        //         unlink($image_path);
        //     }
        $image = $request->file('image');
        $name = time() . 'user' . '.' . $image->getClientOriginalExtension();
        $destinationPath = 'user_images';
        $image->move($destinationPath, $name);
        $user->image = $name;

        $user->update();

        return back()->with('success', trans('app.profile_edit_success_msg'));
    }

    public function profileEditPost($id = null, Request $request)
    {



        if (config('app.is_demo')) {
            return redirect()->back()->with('error', 'This feature has been disable for demo');
        }

        $user = Auth::user();
        if ($id) {
            $user = User::find($id);
        }
        //Validating
        $rules = [
            'email'    => 'required|email|unique:users,email,' . $user->id,
        ];
        $this->validate($request, $rules);

        $inputs = array_except($request->input(), ['_token', 'photo']);
        $user->update($inputs);

        return back()->with('success', trans('app.profile_edit_success_msg'));
    }


    public function changePassword()
    {
        $title = trans('app.change_password');
        return view('admin.change_password', compact('title'));
    }

    public function changePasswordPost(Request $request)
    {
        if (config('app.is_demo')) {
            return redirect()->back()->with('error', 'This feature has been disable for demo');
        }
        $rules = [
            'old_password'  => 'required',
            'new_password'  => 'required|confirmed',
            'new_password_confirmation'  => 'required',
        ];
        $this->validate($request, $rules);

        $old_password = $request->old_password;
        $new_password = $request->new_password;
        //$new_password_confirmation = $request->new_password_confirmation;

        if (Auth::check()) {
            $logged_user = Auth::user();

            if (Hash::check($old_password, $logged_user->password)) {
                $logged_user->password = Hash::make($new_password);
                $logged_user->save();
                return redirect()->back()->with('success', trans('app.password_changed_msg'));
            }
            return redirect()->back()->with('error', trans('app.wrong_old_password'));
        }
    }
}

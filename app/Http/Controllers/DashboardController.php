<?php

namespace App\Http\Controllers;

use App\Job;
use App\User;
use App\Payment;
use App\CompanyRequest;
use App\JobApplication;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard()
    {

        $requests = CompanyRequest::where('status', '=', 0);
        $requestcount = $requests->count();

        $data = [
            'usersCount' => User::count(),
            'totalPayments' => Payment::success()->sum('amount'),
            'activeJobs' => Job::active()->count(),
            'totalJobs' => Job::count(),
            'employerCount' => User::employer()->count(),
            'agentCount' => User::agent()->count(),
            'totalApplicants' => JobApplication::count(),
            'requestcount' =>  $requestcount,

        ];


        return view('admin.dashboard', $data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Industry;
use Illuminate\Http\Request;

class IndustoryController extends Controller
{
    public function index()
    {
        $title = trans('Industries');
        $industries = Industry::orderBy('id', 'desc')->get();

        return view('admin.industries', compact('title', 'industries'));
    }

    public function store(Request $request)
    {
        $rules = [
            'industry_name' => 'required',
        ];
        $this->validate($request, $rules);

        $slug = str_slug($request->industry_name);
        $duplicate = Industry::where('industry_slug', $slug)->count();
        if ($duplicate > 0) {
            return back()->with('error', trans('app.category_exists_in_db'));
        }

        $data = [
            'industry_name' => $request->industry_name,
            'industry_slug' => $slug,
        ];

        Industry::create($data);
        return back()->with('success', trans('industry created'));
    }


    public function destroy($id)
    {
        $industry = Industry::find($id);
        $industry->delete();
        return back()->with(['success' => 'industry Deletet Successfully']);
    }
}

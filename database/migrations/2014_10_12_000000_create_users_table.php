<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');

            $table->integer('country_id')->nullable();
            $table->string('country_name')->nullable();
            $table->integer('state_id')->nullable();
            $table->string('state_name')->nullable();
            $table->string('city')->nullable();
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->string('address')->nullable();
            $table->string('address_2')->nullable();
            $table->string('website')->nullable();
            $table->string('phone')->nullable();
            $table->string('photo')->nullable();
            $table->string('user_code')->nullable();
            $table->string('file')->nullable();
            $table->enum('user_type', ['user', 'employer', 'agent', 'admin']);

            $table->string('company')->nullable();
            $table->string('company_slug')->nullable();
            $table->string('company_size', 5)->nullable();
            $table->text('about_company')->nullable();
            $table->string('logo')->nullable();
            $table->string('image')->nullable();
            $table->string('job_type')->nullable();
            $table->string('industry')->nullable();
            $table->string('profiletype')->nullable();
            $table->string('u_nationality')->nullable();
            $table->longText('u_experience')->nullable();
            $table->longText('u_about')->nullable();
            $table->longText('u_ehistory')->nullable();
            $table->text('skills')->nullable();
            $table->string('exp_level')->nullable();




            $table->integer('premium_jobs_balance')->default(0)->nullable();
            //active_status 0:pending, 1:active, 2:block;
            $table->tinyInteger('active_status')->default(0);
            $table->string('company_post')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

@extends('layouts.theme')

@section('content')

    <div class="blog-listing-page ">

        <div class="blog-listing-header ">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <h1>How It Work</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
              <div class="col  mt-5 mb-5">
                <h1 style="text-align: center">How It Works (website)</h1>
                <p class="text-justify" style="font-size: 18px; letter-spacing: 0.5px;">
                  Matching Yu Association is a non-profit organization, with the aim to achieve a form of cooperation or employment between  skilled professionals , small companies / startups  in poorer or  emerging countries with companies from the stronger economies.. Matching Yu is not a party in this, it is the platform for the first acquintance, where both sides can meet with any obligation. The use and the guidance of Matching Yu is free for both sides.
                  Registered  skilled professionals, small companies and startups can use the platform to expose themselve, and apply for any kind of employment or cooperation.
                  Cooperation means for companies  a sponsorship, technical support, or internships for employees, support with market access, outsourcing etc. For professionals it depends on whether they want to work in their own country or abroad, it can also mean internships, working from home, any kind of a temporary or permanent employment contract, etc.
                  
                  , get procedures are free of charge and are largely automatic via the website, with possible guidance at minimal costs. By exposing themselves on the website in order to achieve a form of cooperation or employment.<br><br>
                  <b>Application</b> <br><br>
                  The procedure  via the website is largely automatic. After registration a file can be made, which will be published after approval. If company, or skilled professional, with the correct chosen classification applicants can be easily found  in the search lists. All visitorss have access to the search lists, but only companies and organisations have on request  access to the complete files. An extract of the file with just the main data (without foto) will be visable on the search list. The applicant-company/startup has the choice to allow Matching Yu to handover the complete file, or to have an agent/mediator been switched between.<br> Applicant professionals also may chose to cover name, gender  and foto in their file, to make it public in the contact themselve. In this case, on request companies will directly receive the complete file, without foto, name and gender. For company- and individual applicants, the first contact from interested companies will always be initiated via the website. 
                  The service Matching Yu offers is free. As Matching Yu is a volontary lead organisation, it highly depends on sponsors and donations. Any cost of placement service, if made,  will be kept very low and will only serve the maintenance and crew of the Matching Yu website. <br>Just in case applicant individuals or companies want a protection and placement service to be carried out by Matching Yu, this service will be charged to the interested company. However, this service is not yet available, so in case placementservice is desired, applicants  will have to turn to commercial placement service organizations
                  Matching Yu applies strict regulations regarding confidentiality. Copying files from applicant individuals or companies, just like commercial use of these data is not allowed and will lead to immedeately termination of the registration.

                  </p>

                  <h1 class="mt-5" style="text-align: center">How it Works  -  Registration  (for applicant indivduals or companies)</h1>
                  <p class="text-justify" style="font-size: 18px; letter-spacing: 0.5px;">
                  <b>Individuals</b><br> Matchingyu, abbreviated MY, does not distinguish between sex, religion and ethnicity. From the information registered AT MY, only the relevant part of your profile will be published. If a company is interested in a profile, the full profile will be released to the searching party automatically/or the applicant or representative must act in case of a request for complete information..
                  </p>
                  <p class="text-justify" style="font-size: 18px; letter-spacing: 0.5px;">
                      <b>  Companies</b><br> Matchingyu, abbreviated MY, distinguishes when registering, whether it concerns sole proprietorships, small and medium-sized enterprises or startups.  After verification by MY  the application will be published in the directory. The Company acts through its CEO or its agent and must choose at the time of registration, whether the complete file (with the name and location of the company), is published or to be released to the searching party after its decision. In case the provider choses to make a limited profile visible (without a name and location), the applicant or its representative itself must act further in case of a request for information. 
                  </p>
                  <p class="text-justify" style="font-size: 18px; letter-spacing: 0.5px;"><b>Searching a professional/company</b> <br> Companies interested in offered professionals or companies should first register with MatchingYu, abbreviated MY.  Making the first contact with the corresponding professionals or companies runs through the website MatchingYu. The registrar may withhold information partly so that not all information is published openly.  Note there are applicant companies and professionals who wish to be mediated from the website MY, or a representative.  In that case, you will get the relevant file, but also the contact address of the mediator. The availability of the complete, or partial file depends on the candidate. It is up to the candidate, whether the request for the complete file can be immedeately  accepted and forwarded from MY, or first through the candidate's intervention. This process is automatic on line. As soon as an employer has made it known that he wants to view a specific file, MY sends the file by return mail/ candidate gets a notification of the request. You will receive the requested file by mutual consent of the candidate within 24 hours.</p>

                  <p class="text-justify" style="font-size: 18px; letter-spacing: 0.5px;">
                       <b> CV  Professionals</b> <br> You don't get a second chance for the first impression. That is why the CV is extremely important. It summarizes succinctly your professional experience and successes in one document and is thus the most important basis for your next career step.     
                       name + marital status  + education   + foto (senior high school, study +finished study level)  year obtained, name institution   your specialities  your work experience  (apprenticeship, research, job) your skills (incl. languages)  hobbies and interests  (what do you want, what don’t references                                                                                                                                                                          (prepare an online file with copies of your diploms and work certificates)                                                                                                                                                   desired outsourcingjob, apprenticeship, research, foreign trainee, duration, willingness to go abroad

                       Employer  offering job ;   companyname, address, tel. Emailaddress, foreign workers occupation if given a work permit Foreign workers main duties if given a work permit                                                                                                                     offered saalry, offered employment status , duration employment                                                                                                                        aware of employer country-specific special responsabilities or to fulfill certain special obligations 

                  </p>
                  
                  <p class="text-justify" style="font-size: 18px; letter-spacing: 0.5px;"> <b>CP, Company Profile </b> <br>                                                                                                              What applies to applicants, certainly also applies to companies and startups, who want to be noticed to take on a form of co-operation with a larger (foreign) partner. Therefore clearly name  corporate credentials, company structure  and covered and potential activities. But always keep in mind to dose specifications and be careful not to reveal the company secrets.    
                        company, company-form                                                                                                                                                                       covered company actiity                                                                                                                                                             number of employees                                                                                                                                                                   turnover                                                                                                                                                                                    specific  notable facts                                                                                                                                                                   references or available publications  what kind of cooperation is persued, c.q. not persued
                        </p>
              </div>
            </div>
          </div>
  
        </div>

@endsection
@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
            <table class="table table-bordered">
                 

                <tr>
                    <th>Company</th>
                    <th>Company Email</th>
                    <th>Action</th>
                </tr>

                @foreach($requestUsers as $requestUser)
                @if($requestUser->status != 2)
                    <tr>
                        <td>
                              {{$requestUser->company}}
                        </td>
                        <td>
                              {{$requestUser->company_email}}
                        </td>
                        <td>
                             
                              @if($requestUser->status == 0 )
                              <a href="{{route('aprove', $requestUser->id)}}" type="button" class="btn btn-success text-white">Aprove</a>
                              <a href="{{route('reject', $requestUser->id)}}" type="button" class="btn text-white btn-danger">Reject</a>

                              @else
                              <a type="button" class="btn btn-dark text-white " dissable>Aproved</a>
                              @endif
                           
                            
                        </td>

                    </tr>
                    @endif
                @endforeach

            </table>


            {!! $requestUsers->links() !!}

        </div>
    </div>



@endsection
@extends('layouts.theme')

@section('content')

<style>
#about h1 {
  margin: 25px;
}
section {
  padding-bottom: 40px;
}
.card .card-header {
  cursor: pointer;
}
</style>

    <div class="blog-listing-header ">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 text-center">
                    <h1>Request Form</h1>
                </div>
            </div>
        </div>
    </div>
<div class="container mt-5 mb-5 bg-white">
    <div class="row">
      <div class="col-md-12 mt-5 mb-5">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif

          <form action="{{route('requestform', ['id' => $jobseeker->id])}}" method="post" enctype="multipart/form-data">
              @csrf
              
              <div class="form-group row {{ $errors->has('company')? 'has-error':'' }}">
                  <label for="address" class="col-sm-4 control-label">@lang('app.company')</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="company" value="{{ old('company')? old('company') : $user->company }}" name="company" placeholder="@lang('app.company')">
                      {!! e_form_error('company', $errors) !!}
                  </div>
              </div>

              <div class="form-group row {{ $errors->has('email')? 'has-error':'' }}">
                  <label for="email" class="col-sm-4 control-label">Companty Email</label>
                  <div class="col-sm-8">
                      <input type="email" class="form-control" id="email" value="{{ old('email')? old('email') : $user->email }}" name="email" placeholder="@lang('app.email')">
                      {!! e_form_error('email', $errors) !!}
                  </div>
              </div>

              <div class="form-group row {{ $errors->has('jobseeker')? 'has-error':'' }}">
                  <label for="jobseeker" class="col-sm-4 control-label">Job Seeker</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="jobseeker" value="{{ old('jobseeker')? old('jobseeker') : $jobseeker->name }}" name="jobseeker">
                      {!! e_form_error('jobseeker', $errors) !!}
                  </div>
              </div>
              <hr />

              <div class="form-group row">
                  <div class="col-sm-8 col-sm-offset-4">
                      <button type="submit" class="btn btn-danger text-white">Submit</button>
                  </div>
              </div>


          </form>



      </div>
  </div>
</div>
@endsection 
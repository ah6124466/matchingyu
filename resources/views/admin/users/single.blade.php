@extends('layouts.theme')

 @section('content') 

<style>
#about h1 {
  margin: 25px;
}
section {
  padding-bottom: 40px;
}
.card .card-header {
  cursor: pointer;
}
</style>

    <div class="blog-listing-header ">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 text-center">
                    <h1>User Profile</h1>
                </div>
            </div>
        </div>
    </div>



    <div class="container mt-5">  
            <!--| ABOUT |--------------------------------------------------->
            <section id="about" class="container"  style="text-align: center">
                  @if($user->profiletype == 0)
                  <img style="border-radius: 50%; width:200px;" src="{{asset('user_images/'.$user->image)}}">
                  @else
                  <img style="border-radius: 20%; width:200px;" src="{{asset('user_images/profile.png')}}">
                  @endif
              
              <h1 class="display-4">{{$user->name}}</h1>
             <p>{{$user->u_about}}</p>
             <p>
                  <strong>Industry:</strong>
                  <span class="badge badge-info">Software Engineer</span>
                
             </p>
              <p>
                <strong>Professional:</strong>
                <span class="badge badge-info">{{$user->skills}}</span>
              </p>
              @if($user->profiletype == 0)
              <p>
                  <a href="{{route('download', $user->id)}}"  class="btn btn-large "><i class="icon-download-alt"> </i> Download CV </a>
                  {{-- <input class="btn btn-danger btn-sm" type="button" value="Download CV"> --}}
                
                  </p>
                  @endif
             
            </section>
       
            
       
            <!-- User Info --------------------------------------------->
            <section id="experience" class="container mb-3">
              <h1>User Info</h1>
              <div class="card">
                <div class="card-header collapse show" data-toggle="collapse" data-target="#exp1">
                  <div class="row">
                    <h5 class="col-md-8 mb-0">Experience</h5>
                    <em class="col-md-4 text-md-right">{{$user->exp_level}}</em>
                  </div>
                </div>
                <div class="card-block collapse" id="exp1">
                  <h5 class="ml-2">{{$user->name}}</h5>
                 <p class="ml-2">{{$user->u_experience}}</p>
                </div>
              </div>
              
              <div class="card">
                <div class="card-header" data-toggle="collapse" data-target="#exp2">
                  <div class="row">
                    <h5 class="col-md-8 mb-0">Education</h5>
                    
                  </div>
                </div>
                <div class="card-block collapse" id="exp2">
                  <h5 class="ml-2"> {{$user->name}}</h5>
                  <p class="ml-2"> {{$user->u_experience}}</p>
                </div>
              </div>
              @if ($user->profiletype == 0)
              <div class="card">
                  <div class="card-header" data-toggle="collapse" data-target="#exp3">
                    <div class="row">
                      <h5 class="col-md-8 mb-0">Address</h5>
                    </div>
                  </div>
                  <div class="card-block collapse" id="exp3">
                    <p class="ml-2"><b>Country:</b> {{$user->country_name}}</p>
                    <p class="ml-2"><b>City:</b> {{$user->city}}</p>
                  </div>
                </div>

              <div class="card">
                <div class="card-header" data-toggle="collapse" data-target="#exp3">
                  <div class="row">
                    <h5 class="col-md-8 mb-0">Contact</h5>
                  </div>
                </div>
                <div class="card-block collapse" id="exp3">
                  <p class="ml-2"><b>Phone:</b> {{$user->phone}}</p>
                  <p class="ml-2"><b>Email:</b> {{$user->email}}</p>
                </div>
              </div>
              @endif
              @if ($user->profiletype == 1)
              <a href="{{route('request',$user->id)}}" class="btn btn-danger mt-2" type="button" >More Info</a>
            @endif
            </section>
      
           
        
          
    </div>

@endsection 
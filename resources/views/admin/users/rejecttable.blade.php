@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
            <table class="table table-bordered">

                <tr>
                    <th>Job Seeker</th>
                    <th>Company</th>
                    <th>Company Email</th>
               
                </tr>

                @foreach($reject_users as $reject_user)
                    <tr>
                        <td>
                             <a href="{{route('requestUser', $reject_user->job_seeker_id)}}" > {{$reject_user->job_seeker}}</a>
                        </td>

                        <td>
                              {{$reject_user->company}}
                        </td>
                        <td>
                              {{$reject_user->company_email}}
                        </td>
                  

                    </tr>
                @endforeach

            </table>


            {!! $reject_users->links() !!}

        </div>
    </div>



@endsection
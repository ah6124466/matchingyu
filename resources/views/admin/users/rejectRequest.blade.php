@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
            <table class="table table-bordered">
                 

                <tr>
                    <th>Company</th>
                    <th>Company Email</th>
                  
                </tr>

                @foreach($company as $c)
                @if($c->status = 2)
                    <tr>
                        <td>
                              {{$c->company}}
                        </td>
                        <td>
                              {{$c->company_email}}
                        </td>
                   

                    </tr>
                    @endif
                @endforeach

            </table>


            {!! $company->links() !!}

        </div>
    </div>



@endsection
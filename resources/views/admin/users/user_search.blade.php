@extends('layouts.theme')

@section('content')


<div class="blog-listing-header ">
      <div class="container">
            <div class="row">
                  <div class="col-md-8 offset-md-2 text-center">
                        <h1>Industry Or Professional: {{$user_ind}}</h1>
                        <h1>Location: {{$user_location}}</h1>
                        <h5>Filter from the left sidebar to find User</h5>
                  </div>
            </div>
      </div>
</div>



<div class="container">
      <div class="row">

            <div class="col-md-4">


                  <div class="jobs-filter-form-wrap bg-white p-4 mt-4 mb-4">

                        <h4 class="mb-4">@lang('app.filter_jobs')</h4>

                        <form action="{{route('filter_search_single')}}" method="get">

                              <div class="form-group">
                                  <p class="text-muted mb-1">@lang('app.keywords')</p>
                                  <input type="text" name="industry" value="" class="form-control" style="min-width: 300px;" placeholder="Industry or Professional">
                              </div>
      
                              <div class="form-group">
                                  <p class="text-muted mb-1">@lang('app.exp_level')</p>
      
                                  <select class="form-control" name="exp_level" id="exp_level">
                                      <option value="" >@lang('app.select_exp_level')</option>
                                      <option value="Medium" >@lang('app.mid')</option>
                                      <option value="Entry">@lang('app.entry')</option>
                                      <option value="Senior">@lang('app.senior')</option>
                                  </select>
                              </div>
      
      
                              <div class="form-group">
                                  <p class="text-muted mb-1">@lang('app.job_type')</p>
      
                                  <select class="form-control" name="job_type" id="job_type">
                                      <option value="">@lang('app.select_job_type')</option>
                                      <option value="full_time" {{ request('job_type') == 'full_time' ? 'selected':'' }}>@lang('app.full_time')</option>
                                      <option value="internship" {{ request('job_type') == 'internship' ? 'selected':'' }}>@lang('app.internship')</option>
                                      <option value="part_time" {{ request('job_type') == 'part_time' ? 'selected':'' }}>@lang('app.part_time')</option>
                                      <option value="contract" {{ request('job_type') == 'contract' ? 'selected':'' }}>@lang('app.contract')</option>
                                      <option value="temporary" {{ request('job_type') == 'temporary' ? 'selected':'' }}>@lang('app.temporary')</option>
                                      <option value="commission" {{ request('job_type') == 'commission' ? 'selected':'' }}>@lang('app.commission')</option>
                                  </select>
                              </div>
      
      
                              <div class="form-group">
                                  <p class="text-muted mb-1">@lang('app.country')</p>
      
                                  <select name="location" class="form-control {{e_form_invalid_class('location', $errors)}} country_to_state">
                                      <option value="">@lang('app.select_a_country')</option>
                                      @foreach($countries as $country)
                                          <option value="{!! $country->country_name !!}" @if(request('country') && $country->id == request('country')) selected="selected" @endif  >{!! $country->country_name !!}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <div class="form-group">
                                  <button type="submit" class="btn btn-danger"><i class="la la-search"></i>Filter Users</button>
                                  {{-- <a href="" class="btn btn-info text-white"><i class="la la-eraser"></i> @lang('app.clear_filter')</a> --}}
                              </div>
                          </form>

                  </div>

            </div>

            <div class="col-md-8">

                  <div class="employer-job-listing mt-4">

                        @if($user->count())

                        <div class="job-search-stats bg-white mb-3 p-4">
                              Total Users: {!! '<strong>', $user->count(), '</strong>' !!}
                        </div>

                        @foreach($user as $u)
                        <div class="employer-job-listing-single box-shadow bg-white mb-3 p-3">


                              <div class="job-listing-company-logo">
                                    <a href="{{route('job_view', $u->name)}}">
                                          <img src="" class="img-fluid" />
                                    </a>
                              </div>


                              <div class="listing-job-info">

                                    <h5><a href="{{route('single-user', $u->id)}}">{{ $u->name }}</a> </h5>

                                    <p class="text-muted mb-1 mt-1">
                                          <i class="la la-th-list">Industry: Software Engineer</i>
                                          <i class="la la-money">Professional: {{$u->skills}}</i>
                                          <i class="la la-th-list">Experience Level: {{$u->exp_level}}</i>


                                    </p>

                                    <p class="text-muted">

                                          <i class="la la-briefcase">Job Type: {{$u->industry}}</i>
                                          <i class="la la-map-marker"></i>
                                          @if($u->country_name)
                                          Address: {!! $u->country_name !!},
                                          @endif
                                          @if($u->city)
                                          {!! $u->city !!}
                                          @endif


                                    </p>

                              </div>


                        </div>

                        @endforeach



                        @else


                        <div class="no-search-results-wrap text-center">

                              <p class="p-4">
                                    <img src="{{asset('assets/images/no-search.png')}}" />
                              </p>

                              <h3>Whoops, no mathces</h3>
                              <h5 class="text-muted">We couldn't find any search results. </h5>
                              <h5 class="text-muted">Give it another try</h5>

                        </div>

                        @endif

                        {{-- {!! $jobs->links() !!} --}}

                  </div>

            </div>
      </div>
</div>

@endsection
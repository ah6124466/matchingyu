@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
            <table class="table table-bordered">

                <tr>
                    <th>Job Seeker</th>
                    <th>Company</th>
                    <th>Company Email</th>
                    <th>Action</th>
                </tr>

                @foreach($requests as $request)
                    <tr>
                        <td>
                             <a href="{{route('requestUser', $request->job_seeker_id)}}"> {{$request->job_seeker}}</a>
                        </td>

                        <td>
                              {{$request->company}}
                        </td>
                        <td>
                              {{$request->company_email}}
                        </td>
                        <td>
                             
                              @if($request->status == 0 )
                              <a href="{{route('aprove', $request->id)}}" type="button" class="btn btn-success text-white">Aprove</a>
                              <a href="{{route('reject', $request->id)}}" type="button" class="btn text-white btn-danger">Reject</a>

                              @else
                              <a type="button" class="btn btn-dark text-white " dissable>Aproved</a>
                              @endif
                           
                            
                        </td>

                    </tr>
                @endforeach

            </table>


            {!! $requests->links() !!}

        </div>
    </div>



@endsection
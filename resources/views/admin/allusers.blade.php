@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
            <table class="table table-bordered">

                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>User Code</th>
                  <th>Action</th>
                </tr>
               
                @foreach($users as $user)
                @if($user->active_status !=2 )
                    <tr>
                        <td>
                             <a> {{$user->name}}</a>
                        </td>

                        <td>
                              {{$user->email}}
                        </td>
                        <td>
                              {{$user->user_code}}
                        </td>
                        <td>               
                              <a href="{{route('viewUser', $user->id)}}" type="button" class="btn btn-info text-white"><i class="la la-eye"></i></a>
                              <a href="{{route('blockUser', $user->id)}}"  type="button" class="btn text-white btn-danger"><i class="la la-close"></i></a>
                              <a href="{{route('deleteUser', $user->id)}}" type="button" class="btn btn-success text-white "><i class="la la-trash"></i></a> 
                        </td>

                    </tr>
                    @endif
                @endforeach

            </table>
            

            {!! $users->links() !!}

        </div>
    </div>



@endsection
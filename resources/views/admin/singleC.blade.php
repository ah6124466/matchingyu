@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
       
        <img style="border-radius: 50%; width:200px; " class=" mx-auto d-block" src="{{asset('user_images/'.$company->image)}}">
        <p class="mt-2">
            <a href="{{route('download', $company->id)}}"  class="btn btn-danger "><i class="icon-download-alt"> </i> Download CV </a>
          
            </p>
        <table class="table table-bordered table-striped mb-4">

            <tr>
                <th>@lang('app.name')</th>
                <td>{{ $company->company }}</td>
            </tr>

            <tr>
                <th>@lang('app.email')</th>
                <td>{{ $company->email }}</td>
            </tr>
            <tr>
                <th>Company Post</th>
                <td>{{ ucfirst($company->company_post) }}</td>
            </tr>
            <tr>
                <th>@lang('app.phone')</th>
                <td>{{$company->phone}}</td>
            </tr>
            <tr>
                <th>@lang('app.address')</th>
                <td>{{$company->address}}</td>
            </tr>
            <tr>
                  <th>Industry</th>
                  <td>{{$company->industry}}</td>
              </tr>
            <tr>
                <th>@lang('app.country')</th>
                <td>
                    {{$company->country_name}}
                </td>
            </tr>

            <tr>
                <th>@lang('app.created_at')</th>
                <td>{{ $company->signed_up_datetime() }}</td>
            </tr>
            <tr>
                <th>@lang('app.status')</th>
                <td>{{ $company->status_context() }}</td>
            </tr>
            <tr>
                  <th>Action</th>
                  <td>
                        <a href="{{route('blockCompany', $company->id)}}" type="button" class="btn text-white btn-danger"><i class="la la-close"></i></a>
                        <a href="{{route('unblockCompany', $company->id)}}" type="button" class="btn btn-danger text-white "><i class="la la-check-circle"></i></a> 
                        <a href="{{route('deleteCompany', $company->id)}}" type="button" class="btn btn-success text-white "><i class="la la-trash"></i></a>
                  </td>
              </tr>
        </table>

        </div>
    </div>



@endsection
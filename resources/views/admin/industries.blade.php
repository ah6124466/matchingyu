@extends('layouts.dashboard')

@section('content')

    <div class="row">
        <div class="col-md-10">


            <form method="post" action="{{route('industry_store')}}">
                @csrf

                <div class="form-group row">
                    <label for="industry_name" class="col-sm-4 control-label">Industry Name</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control " id="industry_name" value="" name="industry_name" placeholder="industry name">

                        {!! e_form_error('industry_name', $errors) !!}
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-danger">Save New Industry</button>
                    </div>
                </div>

            </form>

        </div>

    </div>

    <div class="row">
      <div class="col-md-12">
          <table class="table table-bordered">
              <tr>
                  <th>Industry Name</th>
                  <th>Action</th>
              </tr>
              @foreach($industries as $industry)
                  <tr>
                      <td>
                          {{ $industry->industry_name }}
                      </td>
                      <td>
                          <a href="{{ route('delete_industries', $industry->id) }}" class="btn btn-danger"><i class="la la-trash"></i> </a>
                      </td>
                  </tr>
              @endforeach
          </table>
      </div>
  </div>



@endsection
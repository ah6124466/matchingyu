@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
            <table class="table table-bordered">

                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Company Code</th>
                  <th>Action</th>
                </tr>

                @foreach($companies as $company)
                @if($company->active_status == 2 && $company->user_type == 'employer' )
                    <tr>
                        <td>
                             <a> {{$company->company}}</a>
                        </td>

                        <td>
                              {{$company->email}}
                        </td>
                        <td>
                              {{$company->user_code}}
                        </td>
                        <td>               
                              <a href="{{route('viewCompany', $company->id)}}" type="button" class="btn btn-info text-white"><i class="la la-eye"></i></a>
                              <a href="{{route('unblockCompany', $company->id)}}" type="button" class="btn btn-danger text-white "><i class="la la-check-circle"></i></a> 
                              <a href="{{route('deleteCompany', $company->id)}}" type="button" class="btn btn-danger text-white "><i class="la la-trash"></i></a> 
                        </td>

                    </tr>
                    @endif
                @endforeach

            </table>


            {!! $companies->links() !!}

        </div>
    </div>



@endsection
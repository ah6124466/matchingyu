@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
       
        <img style="border-radius: 50%; width:200px; " class=" mx-auto d-block" src="{{asset('user_images/'.$user->image)}}">
        <p class="mt-2">
            <a href="{{route('download', $user->id)}}"  class="btn btn-danger "><i class="icon-download-alt"> </i> Download CV </a>
          
            </p>
        <table class="table table-bordered table-striped mb-4">

            <tr>
                <th>@lang('app.name')</th>
                <td>{{ $user->name }}</td>
            </tr>

            <tr>
                <th>@lang('app.email')</th>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <th>@lang('app.gender')</th>
                <td>{{ ucfirst($user->gender) }}</td>
            </tr>
            <tr>
                <th>@lang('app.phone')</th>
                <td>{{$user->phone}}</td>
            </tr>
            <tr>
                <th>@lang('app.address')</th>
                <td>{{$user->address}}</td>
            </tr>
            <tr>
                  <th>Professional</th>
                  <td>{{$user->skills}}</td>
              </tr>
            <tr>
                <th>@lang('app.country')</th>
                <td>
                    {{$user->country_name}}
                </td>
            </tr>

            <tr>
                <th>@lang('app.created_at')</th>
                <td>{{ $user->signed_up_datetime() }}</td>
            </tr>
            <tr>
                <th>@lang('app.status')</th>
                <td>{{ $user->status_context() }}</td>
            </tr>
            <tr>
                  <th>Action</th>
                  <td>
                        <a href="{{route('blockUser', $user->id)}}"  type="button" class="btn text-white btn-danger"><i class="la la-close"></i></a>
                        <a href="{{route('unblockUser', $user->id)}}" type="button" class="btn btn-danger text-white "><i class="la la-check-circle"></i></a> 
                        <a href="{{route('deleteUser', $user->id)}}"  type="button" class="btn btn-success text-white "><i class="la la-trash"></i></a>
                  </td>
              </tr>
        </table>

        </div>
    </div>



@endsection
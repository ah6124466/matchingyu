@extends('layouts.dashboard')


@section('content')
    <div class="row">
        <div class="col-md-12">


            <form action="{{route('UserUpdate' ,$user->id)}}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="form-group row {{ $errors->has('name')? 'has-error':'' }}">
                    <label for="name" class="col-sm-4 control-label">@lang('app.name')</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" value="{{ old('name')? old('name') : $user->name }}" name="name" placeholder="@lang('app.name')">
                        {!! e_form_error('name', $errors) !!}
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('email')? 'has-error':'' }}">
                    <label for="email" class="col-sm-4 control-label">@lang('app.email')</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" id="email" value="{{ old('email')? old('email') : $user->email }}" name="email" placeholder="@lang('app.email')">
                        {!! e_form_error('email', $errors) !!}
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('gender')? 'has-error':'' }}">
                    <label for="gender" class="col-sm-4 control-label">@lang('app.gender')</label>
                    <div class="col-sm-8">
                        <select id="gender" name="gender" class="form-control select2">
                            <option value="">Select Gender</option>
                            <option value="male" {{ $user->gender == 'male'?'selected':'' }}>Male</option>
                            <option value="female" {{ $user->gender == 'female'?'selected':'' }}>Fe-Male</option>
                            <option value="third_gender" {{ $user->gender == 'third_gender'?'selected':'' }}>Third Gender</option>
                        </select>
                        {!! e_form_error('gender', $errors) !!}
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('industry')? 'has-error':'' }}">
                    <label for="industry" class="col-sm-4 control-label">Jobe Type</label>
                    <div class="col-sm-8">
                        <select id="industry" name="job_type" class="form-control select2">
                            <option value="">@lang('app.select_job_type')</option>
                            <option value="full_time" {{ request('job_type') == 'full_time' ? 'selected':'' }}>@lang('app.full_time')</option>
                            <option value="internship" {{ request('job_type') == 'internship' ? 'selected':'' }}>@lang('app.internship')</option>
                            <option value="part_time" {{ request('job_type') == 'part_time' ? 'selected':'' }}>@lang('app.part_time')</option>
                            <option value="contract" {{ request('job_type') == 'contract' ? 'selected':'' }}>@lang('app.contract')</option>
                            <option value="temporary" {{ request('job_type') == 'temporary' ? 'selected':'' }}>@lang('app.temporary')</option>
                            <option value="commission" {{ request('job_type') == 'commission' ? 'selected':'' }}>@lang('app.commission')</option>
                        
                        </select>
                        {!! e_form_error('industry', $errors) !!}
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('profiletype')? 'has-error':'' }}">
                    <label for="profiletype" class="col-sm-4 control-label">Pofile Type</label>
                    <div class="col-sm-8">
                        <select id="profiletype" name="profiletype" class="form-control select2" required>
                            <option value="">Select Type</option>
                            <option value="0" >Public</option>
                            <option value="1">Private</option>
                            
                        </select>
                        {!! e_form_error('profiletype', $errors) !!}
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('phone')? 'has-error':'' }}">
                    <label for="phone" class="col-sm-4 control-label">@lang('app.phone')</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="phone" value="{{ old('phone')? old('phone') : $user->phone }}" name="phone" placeholder="@lang('app.phone')">
                        {!! e_form_error('phone', $errors) !!}
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('country_id')? 'has-error':'' }}">
                    <label for="phone" class="col-sm-4 control-label">@lang('app.country')</label>
                    <div class="col-sm-8">
                        <select id="country_id" name="country_name" class="form-control select2">
                            <option value="">@lang('app.select_a_country')</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->country_name }}" {{ $user->country_id == $country->id ? 'selected' :'' }}>{{ $country->country_name }}</option>
                            @endforeach
                        </select>
                        {!! e_form_error('country_id', $errors) !!}
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('city')? 'has-error':'' }}">
                    <label for="city" class="col-sm-4 control-label">City</label></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="city" value="{{ old('city')? old('city') : $user->city }}" name="city" placeholder="city">
                        {!! e_form_error('city', $errors) !!}
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('u_nationality')? 'has-error':'' }}">
                    <label for="nationality" class="col-sm-4 control-label">Nationality</label></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="u_nationality" value="{{ old('u_nationality')? old('u_nationality') : $user->nationality }}" name="u_nationality" placeholder="nationality">
                        {!! e_form_error('u_nationality', $errors) !!}
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('address')? 'has-error':'' }}">
                    <label for="address" class="col-sm-4 control-label">@lang('app.address')</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="address" value="{{ old('address')? old('address') : $user->address }}" name="address" placeholder="@lang('app.address')">
                        {!! e_form_error('address', $errors) !!}
                    </div>
                </div>


                <div class="form-group row {{ $errors->has('industry')? 'has-error':'' }}">
                  <label for="industry" class="col-sm-4 control-label">Industry</label>
                  <div class="col-sm-8">
                    {{-- <input type="text" class="form-control" id="industry" value="{{ old('industry')? old('industry') : $user->industry }}" name="industry" placeholder="Professional"> --}}
                    {{-- {!! e_form_error('industry', $errors) !!} --}}
                        <select id="industry" name="industry" class="form-control select2">
                              <option value="">Select industry</option>
                              @foreach($industries as $industry)
                                  <option value="{{ $industry->industry_name }}">{{ $industry->industry_name }}</option>
                              @endforeach
                          </select>
                      {!! e_form_error('industry', $errors) !!}
                  </div>
              </div>

              <div class="form-group row {{ $errors->has('category')? 'has-error':'' }}">
                <label for="category" class="col-sm-4 control-label">Professional</label>
                <div class="col-sm-8">
                  {{-- <input type="text" class="form-control" id="industry" value="{{ old('industry')? old('industry') : $user->industry }}" name="industry" placeholder="Professional"> --}}
                  {{-- {!! e_form_error('industry', $errors) !!} --}}
                      <select id="category" name="category" class="form-control select2">
                            <option value="">Select Professional</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->category_name }}">{{ $category->category_name }}</option>
                            @endforeach
                        </select>
                    {!! e_form_error('category', $errors) !!}
                </div>
            </div>

              <div class="form-group row {{ $errors->has('exp_level')? 'has-error':'' }}">
                <label for="exp_level" class="col-sm-4 control-label">Experience Level</label>
                <div class="col-sm-8">
                    <select class="form-control" name="exp_level" id="exp_level">
                        <option value="" >@lang('app.select_exp_level')</option>
                        <option value="Medium" >@lang('app.mid')</option>
                        <option value="Entry">@lang('app.entry')</option>
                        <option value="Senior">@lang('app.senior')</option>
                    </select>
                    {!! e_form_error('exp_level', $errors) !!}
                </div>
            </div>

                <div class="form-group row {{ $errors->has('u_experience')? 'has-error':'' }}">
                    <label for="u_experience" class="col-sm-4 control-label">Experience</label>
                    <div class="col-sm-8">
                      <textarea class="form-control" name="u_experience" id="u_experience" cols="30" rows="5"></textarea>
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('u_about')? 'has-error':'' }}">
                    <label for="u_experience" class="col-sm-4 control-label">About</label>
                    <div class="col-sm-8">
                      <textarea class="form-control" name="u_about" id="u_about" cols="30" rows="5"></textarea>
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('u_ehistory')? 'has-error':'' }}">
                    <label for="u_ehistory" class="col-sm-4 control-label">Education History</label>
                    <div class="col-sm-8">
                      <textarea class="form-control" name="u_ehistory" id="u_ehistory" cols="30" rows="5"></textarea>
                    </div>
                </div>

               

                <div class="form-group row {{ $errors->has('file')? 'has-error':'' }}">
                    <label for="file" class="col-sm-4 control-label">Upload CV</label>
                    <div class="col-sm-8">
                        <input type="file" class="form-control" name="file" value="{{ old('file')? old('file') : $user->file }}" name="file">
                        {!! e_form_error('file', $errors) !!}
                    </div>
                </div>

                <div class="form-group row {{ $errors->has('image')? 'has-error':'' }}">
                    <label for="image" class="col-sm-4 control-label">Upload Image</label>
                    <div class="col-sm-8">
                        <input type="file" class="form-control" name="image" value="{{ old('image')? old('image') : $user->file }}" name="image">
                        {!! e_form_error('image', $errors) !!}
                    </div>
                </div>

               

                <hr />

                <div class="form-group row">
                    <div class="col-sm-8 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary">@lang('app.edit')</button>
                    </div>
                </div>


            </form>



        </div>
    </div>



@endsection
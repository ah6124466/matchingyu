@extends('layouts.theme')

@section('content')

    <div class="blog-listing-page ">

        <div class="blog-listing-header ">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <h1>Privacy Policy</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
              <div class="col mt-5 mb-5">
                  <h1>Matching Yu Association, Privacy Policy</h1>
                  <p class="text-justify mt-3" style="font-size: 18px; letter-spacing: 0.5px;">
                                          
                        Please read this privacy policy (the <b>“Policy”</b>) carefully to understand how we use your personal information. If you do not agree with this Policy, your choice is not to use matchingyu.com (the <b>“Site”</b>). By accessing or using this Site, you agree to this Policy. This Policy may change from time to time. If there are any material changes to how your personal information is used, we will notify you. Your continued use of the Site after we make changes is deemed to be acceptance of those changes, so please check the Policy periodically for update. 
                        At matchingyu.com we care about your privacy. We do not sell or rent your personal information to third parties for their direct marketing purposes without your explicit consent. We do not disclose it to others except as disclosed in this Policy or required to provide you with the services of the Site and mobile applications, meaning - to allow you to buy, sell, share the information you want to share on the Site; to contribute on the forum; pay for products; post reviews and so on; or where we have a legal obligation to do so. 
                        We collect information that you provide us or voluntarily share with other users, and also some general technical information that is automatically gathered by our systems, such as IP address, browser information and cookies to enable you to have a better user experience and a more personalized browsing experience. <br><br>
                        We will not share information that you provide us in the process of the registration - including your contact information - except as described in this Policy and MatchingYu Terms and Conditions 
                        Information that you choose to publish on the Site (photos, videos, text, music, reviews, deliveries) - is no longer private, just like any information you publish online. 
                        Technical information that is gathered by our systems, or third party systems, automatically may be used for Site operation, optimization, analytics, content promotion and enhancement of user experience. We may use your information to contact you - to provide notices related to your activities, or offer you promotions and general updates, but we will not let any other person, including sellers and buyers, contact you, other than through your user profile on the Site. 
                        Your personal information may be stored in systems based around the world, and may be processed by third party service providers acting on our behalf. These providers may be based in countries that do not provide an equivalent level of protection for privacy as that enjoyed in the country in which you live. In that case, we will provide for adequate safeguards to protect your personal information. 
                        You can exercise your rights over your personal information, by contacting us, Matchng Yu Association,                                     .                                       email: info@matchingyu.com
</p>
              </div>
            </div>
          </div>
  
        </div>

@endsection
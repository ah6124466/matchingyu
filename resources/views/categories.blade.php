@extends('layouts.theme')

@section('content')

    <div class="blog-listing-page ">

        <div class="blog-listing-header ">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <h1>Professionals</h1>
                    </div>
                </div>
            </div>
        </div>

        @if($categories->count())
        <div class="home-categories-wrap bg-white pb-5 pt-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="mb-3">@lang('app.browse_category')</h4>
                    </div>
                </div>

                <div class="row" mt-5>

                    @foreach($categories as $category)
                        <div class="col-md-4">

                            <p>
                                <a href="{{route('users_professional', ['category' => $category->category_slug])}}" class="category-link"><i class="la la-th-large"></i> {{$category->category_name}} <span class="text-muted">({{$category->job_count}})</span> </a>
                            </p>

                        </div>

                    @endforeach

            </div>
        </div>
    @endif
  
        </div>

@endsection
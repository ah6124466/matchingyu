@extends('layouts.theme')

@section('content')

    <div class="blog-listing-page ">

        <div class="blog-listing-header ">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <h1>About Us</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-around mt-5">
            <div class="col-4  mt-5">
                <p class="text-justify" style="font-size: 18px; letter-spacing: 0.5px;">
                    <b>Matching Yu for oppertunities and access.</b>
                       Many skilled professionals and small companies/startups in Africa, Latin America, Asia or Eastern Europe are waiting for a form of cooperation. Consequence ?  Further developing oppertunities and future access for both sides.
                  </p>
            </div>
            <div class="col-4  mt-5">
                <img src="{{asset('/assets/images/about1.jpg')}}"  style="width: 500px; height:300px;"  alt="">
                <h6 style="text-align: center;" class="mt-1"> StartUp</h6>
            </div>
          </div>
    


          <div class="row justify-content-around">
            <div class="col-4  mt-5">
                <img src="{{asset('/assets/images/about2.jpg')}}"  style="width: 500px; height:300px;"  alt="">
                <h6 style="text-align: center;" class="mt-1"> Graduate</h6>
            </div>

            <div class="col-4  mt-5">
                <p class="text-justify"  style=" font-size: 18px;">
                    <b>Matching Yu</b> is carried out under the auspices of Fair Trade Association. Ambitious foreign small companies/startups and professionals, looking for a connection in the strongest economies may introduce and expose themselves. Without support or cooperation face companies and startups from not western and emerging markets huge problems to expand in external markets. On the other hand, many western companies are seeking access to these emerging markets.<b>Matching Yu</b>, the platform for a first introduction.
                  </p>
            </div>
          </div>

 
          <div class="row justify-content-around">
            <div class="col-4  mt-5">
               <p class="text-justify"  style="font-size: 18px;"><b>Matching Yu</b>’s consulting will provide very valuable global information about available professionals, new ideas/products, outsourcing solutions from hidden small companies and startups ready for a cooperation.
                .                                                                                                                         Seeking attention, cooperation for support in further development, sponsor for new markets, or even adoption. Even   a take over could be desirable. 
                Matching (or to be found to match) is not easy for foreign companies/startups. <b>Matching Yu</b> is an efficient tool. 
                </p>
            </div>
            <div class="col-4  mt-5">
                <img src="{{asset('/assets/images/about3.jpg')}}"  style="width: 500px; height:300px;"  alt="">
                <h6 style="text-align: center;" class="mt-1">StartUp</h6>
            </div>
          </div>



          <div class="row justify-content-around">
            <div class="col-4  mt-5">
                <img src="{{asset('/assets/images/about4.jpg')}}"  style="width: 500px; height:300px;"  alt="">
                <h6 style="text-align: center;" class="mt-1">Permanent Education</h6>
            </div>

            <div class="col-4  mt-5">
               <p class="text-justify" style="font-size: 18px;">Many talented graduates and skilled workers worldwide are stuck in their countries, lack oppertunities of further development.  Eager to use their acquired expertise for a (temporary) employment or internships abroad or an outsourcing assignment at home to boost their experience.        </p>
            </div>
          </div>
    


          <div class="row justify-content-around">
            <div class="col-4  mt-5">
              <p class="text-justify" style="font-size: 18px;">Despite strict entry policies regarding foreign workers in many countries, these usually do not apply to talented and skilled professionals. The problem is that  structures for such matching often are unknown or insufficient. 

                The emphasis of Matching Yu is fruitful matching, bringing the two worlds together. Whether for two companies, or a worker applying for an internship or employment. Companies from emerging markets can get used to western standards, learn with support how to access new markets. Introducing each other's products in the respective home market offers opportunities for both sides. Professionals, once returned with their acquired knowledge, will build up their countries or origin.
                </p>
            </div>
            <div class="col-4  mt-5">
                <img src="{{asset('/assets/images/about5.jpg')}}"  style="width: 500px; height:300px;"  alt="">
                <h6 style="text-align: center;" class="mt-1">Micro-Business</h6>
            </div>
          </div>
   

          
          <div class="row justify-content-around">
            <div class="col-4  mt-5">
                <img src="{{asset('/assets/images/about6.jpg')}}"  style="width: 500px; height:300px;"  alt="">
                <h6 style="text-align: center;" class="mt-1">International Cooperation</h6>
            </div>

            <div class="col-4  mt-5">
               <p class="text-justify" style="font-size: 18px;">Registration and the use  of the website is approachable and free. The used language of this platform is English. To open the sky, professionals, small companies and startups simply register with information about the offered specialities and workfield in brief.   

                Not forgotten are the periods, many companies struggled with rising productioncosts due to a shortage of labour.  what finally weakened their position in the worldmarket.  Political fear however, blocked an influx of new regular or temporary workers and handicrafts in booming countries. A good solution could be production relief by outsourcing to the less prosperous countries.</p>
          </div>
    

          <div class="row justify-content-around">
            <div class="col-4  mt-5">
              <h4 class="text-justify">Although the world has to recover from the actual crisis, Europe takes more measures to activate the economy in the less prosperous countries, combined with the efforts private companies to train on-site staff and invite a certain number of workers for temporary work placements.                                                                                           

                However Europe, USA, but also Japan or China should be more generous in providing scholarships for talented students from abroad. Showing them how it works in order to create later structured economic activity, would be the best contribution. As Europe, USA, Japan and Korea produce high quality products, they should grant the less prosperous countries a fair market share for goods they can produce, instead of pushing them out of the market or closing them down.                                         .                                                                             European sugar still is highly subsidized, even while there is a surplus of sugar on the world market. However, initial developments are already underway in Africa, like the local processing of cocoa into high-quality chocolate products for export to Europe. 
                </h4>
            </div>
            <div class="col-4  mt-5">
                <img src="{{asset('/assets/images/about7.jpg')}}"  style="width: 500px; height:300px;"  alt="">
                <h6 style="text-align: center;" class="mt-1">Micro-Business</h6>
            </div>
          </div>


          <div class="row justify-content-around">

            <div class="col-4  mt-5 mb-5">
                <img src="{{asset('/assets/images/about8.jpg')}}"  style="width: 500px; height:300px;"  alt="">
                <h6 style="text-align: center;" class="mt-1"> Matching Yu, business directory with worldwide coverage</h6>
            </div>

            <div class="col-4  mt-5 mb-5">
               <p class="text-justify " style="font-size: 18px;"><b>Matching Yu</b> is convinced that a fair trade with a broader economic participation will finaly lead to a better world with ultimately benefits not for Europe/USA/Japan, but most so for the remaining countries. Get to know each other with <b>Matching Yu</b>.  The win-win situation may be clear. </p>
            </div>

        </div
  
        </div>

@endsection